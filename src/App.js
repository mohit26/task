import './App.css';
import React from 'react'
import './assets/css/bootstrap.min.css'
import './assets/css/custom.css'
import {colors, textColor} from './utils'

class App extends React.Component {
    state = {
        backgroundColor: '#ffffff',
        text: '',
        count: 0
    };
    divRef = React.createRef();
    onChangeColor = (color) => {
        // this.divRef.current.innerHTML = "";
        this.setState({
            backgroundColor: color,
            // count: 0
        })
    };
    submitText = () => {
        let canvas = this.divRef.current;
        let elem = document.createElement("h5");
        elem.style.top = this.state.count * 30 + "px";
        elem.innerText = this.state.text;
        canvas.appendChild(elem);
        this.setState({text: "", count: this.state.count + 1})
    };
    reset = () => {
        this.divRef.current.innerHTML = "";
        this.setState({
            backgroundColor: '#ffffff',
            count: 0,
            text: ''
        })
    }
    textChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    };
    handleImage = (e) => {
        // this.reset()
        let canvas = this.divRef.current;
        let reader = new FileReader();
        reader.onload = function (event) {
            let img = new Image();
            img.onload = function () {
                img.width = canvas.clientWidth;
                img.height = canvas.clientHeight;
                img.id = "background-img";
                canvas.appendChild(img);
            };
            img.src = event.target.result;
        };
        reader.readAsDataURL(e.target.files[0]);
        document.getElementById('customFile').value = ""

    };

    render() {
        return (
            <div className="App">
                <div className="container-fluid vh-100">
                    <div className="container vh-100">
                        <div className="row task-row-main">
                            <div className="col-12 col-sm-12 col-md-6">
                                <div ref={this.divRef}
                                     style={{backgroundColor: this.state.backgroundColor}}
                                     className="task-card-row position-relative overflow-hidden"/>
                            </div>
                            <div className="col-12 col-sm-12 col-md-6">
                                <div className="row task-details-row">
                                    <div className="col-12">
                                        <h6>Choose Color:</h6>
                                    </div>
                                    <div className="col-12 mb-2">
                                        <div className="row mx-0 choose-color-row">
                                            {colors.map(d =>
                                                <div title={d} onClick={() => this.onChangeColor(d)}
                                                     style={{backgroundColor: d}}
                                                     className="common-color-main d-inline-block"/>
                                            )}
                                        </div>
                                    </div>
                                    <div className="col-12">
                                        <h6>Upload Image:</h6>
                                    </div>
                                    <div className="col-12">
                                        <div className="custom-file mb-2">
                                            <input onChange={this.handleImage} type="file"
                                                   className="custom-file-input" id="customFile"/>
                                            <label className="custom-file-label" htmlFor="customFile">Choose
                                                file</label>
                                        </div>
                                    </div>
                                    <div className="col-12">
                                        <div className="row">
                                            <div className="col-sm-10">
                                                <h6 className="mb-2">Insert Text:</h6>
                                                <input value={this.state.text} onChange={this.textChange} name="text"
                                                       type="text"
                                                       className="form-control" placeholder="Enter Here"/>
                                            </div>
                                            <div className="col-sm-2">
                                                <button disabled={!this.state.text} onClick={() => this.submitText()}
                                                        type="button" className="btn btn-primary submit-btn"
                                                        style={{cursor: 'pointer'}}>Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 mb-4">
                                        <div className="choose-color-row mt-2">
                                            <h6>Text Color:</h6>
                                            {textColor.map(d =>
                                                <div title={d} onClick={() => this.onChangeColor(d)}
                                                     style={{
                                                         backgroundColor: d,
                                                         border: '1px solid #bbbbbb',
                                                         marginRight: 5
                                                     }}
                                                     className="common-color-main d-inline-block"/>
                                            )}
                                        </div>

                                        <button onClick={() => this.reset()}
                                                type="button" className="btn btn-secondary mt-2"
                                                style={{cursor: 'pointer'}}>Reset
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default App;
